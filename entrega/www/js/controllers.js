angular.module('starter.controllers', [])

.controller('BatteryCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaBatteryStatus, $ionicPopup) {
 
    $scope.isPlugged = "Not charging";
    $scope.level = 100;
    $scope.icon = "ion-battery-full";
    
    // Battery checking is an event!!
    $ionicPlatform.ready(function() {
        $rootScope.$on("$cordovaBatteryStatus:status", function(event, args) {
            $scope.level = args.level;
            if(args.isPlugged) {
                $scope.isPlugged = "Charging";
                $scope.icon = "ion-battery-charging";
            } else {
                $scope.isPlugged = "Not charging";
                if (args.level >= 90) {
                  $scope.icon = "ion-battery-full";
                }
                else if (args.level > 50) {
                  $scope.icon = "ion-battery-half";
                }
                else if (args.level > 15) {
                  $scope.icon = "ion-battery-low";
                }
                else {
                  $scope.icon = "ion-battery-empty"; 
                }
            }
        });
    });

    // "Checks" the battery.
  $scope.checkBattery = function() {
    var batteryAlert = $ionicPopup.alert({
        title: 'Battery Status!',
        template: 'Battery level: ' + $scope.level
    });

    batteryAlert.then(function(res) {
    });
 };
 
})

.controller('NetworkCtrl', function($scope, $ionicPlatform) {
    $scope.message = "No connection";
    $scope.icon = "ion-alert-circled";

    $scope.checkConnection = function() {
      if(window.Connection) {
            if(navigator.connection.type == Connection.NONE) {
                $scope.message = "No connection";
                $scope.icon = "ion-alert-circled";
            }
            else if (navigator.connection.type == Connection.WIFI) {
                $scope.message = "Connected to Wi-Fi";
                $scope.icon = "ion-wifi";
            }
            else {
                $scope.icon = "ion-connection-bars";
                $scope.message = "Connected to network";
                if (navigator.connection.type == Connection.CELL_2G) {
                    $scope.message = "Connected to a 2G network";
                }
                else if (navigator.connection.type == Connection.CELL_3G) {
                    $scope.message = "Connected to a 3G network";
                }
                else if (navigator.connection.type == Connection.CELL_4G) {
                    $scope.message = "Connected to a 4G network";
                }
            }
        }
    }

    $ionicPlatform.ready($scope.checkConnection);

})

.controller('WebCtrl', function($scope, $ionicPlatform) {
    $scope.estudyUrl = "http://estudy.salle.url.edu";
    $scope.gplusUrl = "https://plus.google.com/u/0/102579874642251872230";
    $scope.mdpaUrl = "http://www.beslasalle.net/portal/masters/masters-electronica-mdpa-barcelona/Controller?mvchandler=portals&action=start&showingIndex=true&use-lang=ca&screen=workspace";
});


